package org.vaultage.server.database;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class DatabaseSerializerTest {

  static Database db;
  static DatabaseSerializer<MySerializable> dbSerializer;

  @BeforeClass
  public static void setUp() throws Exception {
    db = new Database("database/test/test", "sa", "", null);
    db.start();
    db.update(db.prepareStatement("DROP TABLE serialize IF EXISTS"));
    db.update(db.prepareStatement("CREATE TABLE IF NOT EXISTS serialize (id INT "
        + "IDENTITY, data LONGVARBINARY)"));
    dbSerializer = new DatabaseSerializer<MySerializable>(db, "serialize");
  }

  @AfterClass
  public static void breakDown() throws SQLException, IOException {
    db.shutdown();
    FileUtils.deleteDirectory(new File("database"));
  }

  @Test
  public void test() throws SQLException {
    MySerializable mySerializable = new MySerializable("asfd", 5, 3.2);
    MySerializable mySerializable2 = new MySerializable("muha", -13451235, 123512.512351);
    assertEquals(0, dbSerializer.write(mySerializable));
    assertEquals(1, dbSerializer.write(mySerializable2));
    assertEquals(mySerializable.toString(), dbSerializer.read(0).toString());
    assertEquals(mySerializable2.toString(), dbSerializer.read(1).toString());
  }

}

class MySerializable implements Serializable {

  private static final long serialVersionUID = 201302031431L;

  String s;
  int i;
  double d;

  public MySerializable(String s, int i, double d) {
    this.s = s;
    this.i = i;
    this.d = d;
  }

  @Override
  public String toString() {
    return "MySerializable [s=" + s + ", i=" + i + ", d=" + d + "]";
  }

}
