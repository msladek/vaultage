package org.vaultage.server.database;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.same;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.vaultage.commons.auth.Auth;
import org.vaultage.commons.auth.AuthResponse;
import org.vaultage.server.comm.UserManager;

public class AuthoriserTest {

  private static Authoriser authoriser;

  private static Database databaseMock;
  private static PreparedStatement pStatementMock;
  private static UserManager userManagerMock;

  @Before
  public void setUp() throws Exception {
    databaseMock = createMock(Database.class);
    pStatementMock = createMock(PreparedStatement.class);
    userManagerMock = createMock(UserManager.class);
    authoriser = new Authoriser(databaseMock, userManagerMock);

    String sql = "select USER_ID from USER where NAME=? and HASH=?";
    expect(databaseMock.prepareStatement(eq(sql))).andReturn(pStatementMock).once();
    pStatementMock.setString(eq(1), eq("test"));
    expectLastCall().once();
    pStatementMock.setString(eq(2), eq(getHash()));
    expectLastCall().once();
  }

  @Test
  public void test_Success() throws SQLException {
    String clientID = "someID";
    Auth auth = new Auth("test", "asdf");
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[] { 5 });
    expect(databaseMock.query(same(pStatementMock))).andReturn(result).once();
    expect(userManagerMock.unlock(eq(clientID), eq(5))).andReturn(true).once();

    replayAll();
    assertEquals(AuthResponse.SUCCESS, authoriser.authorise(auth, clientID));
    verifyAll();
  }

  @Test
  public void test_Failure_null() throws SQLException {
    String clientID = "someID";
    Auth auth = new Auth("test", "asdf");

    expect(databaseMock.query(same(pStatementMock))).andReturn(null).once();

    replayAll();
    assertEquals(AuthResponse.FAILURE, authoriser.authorise(auth, clientID));
    verifyAll();
  }

  @Test
  public void test_Failure_empty() throws SQLException {
    String clientID = "someID";
    Auth auth = new Auth("test", "asdf");
    List<Object[]> result = new ArrayList<>();

    expect(databaseMock.query(same(pStatementMock))).andReturn(result).once();

    replayAll();
    assertEquals(AuthResponse.FAILURE, authoriser.authorise(auth, clientID));
    verifyAll();
  }

  @Test
  public void test_Failure_emptyRow() throws SQLException {
    String clientID = "someID";
    Auth auth = new Auth("test", "asdf");
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[] {});

    expect(databaseMock.query(same(pStatementMock))).andReturn(result).once();

    replayAll();
    assertEquals(AuthResponse.FAILURE, authoriser.authorise(auth, clientID));
    verifyAll();
  }

  @Test
  public void test_Failure_wrongType() throws SQLException {
    String clientID = "someID";
    Auth auth = new Auth("test", "asdf");
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[] { "asdf" });

    expect(databaseMock.query(same(pStatementMock))).andReturn(result).once();

    replayAll();
    assertEquals(AuthResponse.FAILURE, authoriser.authorise(auth, clientID));
    verifyAll();
  }

  @Test
  public void test_Failure_negativeid() throws SQLException {
    String clientID = "someID";
    Auth auth = new Auth("test", "asdf");
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[] { -1 });

    expect(databaseMock.query(same(pStatementMock))).andReturn(result).once();

    replayAll();
    assertEquals(AuthResponse.FAILURE, authoriser.authorise(auth, clientID));
    verifyAll();
  }

  @Test
  public void test_Failure_exception() throws SQLException {
    String clientID = "someID";
    Auth auth = new Auth("test", "asdf");
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[] { -1 });

    expect(databaseMock.query(same(pStatementMock))).andThrow(new SQLException());

    replayAll();
    assertEquals(AuthResponse.FAILURE, authoriser.authorise(auth, clientID));
    verifyAll();
  }

  private String getHash() {
    return "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429080fb337591abd3e4"
        + "4453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
  }

  private void replayAll(Object... mocks) {
    replay(databaseMock, pStatementMock, userManagerMock);
    replay(mocks);
  }

  private void verifyAll(Object... mocks) {
    verify(databaseMock, pStatementMock, userManagerMock);
    verify(mocks);
  }

}
