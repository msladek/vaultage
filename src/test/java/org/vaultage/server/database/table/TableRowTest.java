package org.vaultage.server.database.table;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class TableRowTest {

  @Before
  public void setUp() {

  }

  @Test
  public void test_user() {
    assertNotNull(new TableRow(Table.USER, "myUserName1", "someHash1"));
  }

  @Test
  public void test_tree() {
    assertNotNull(new TableRow(Table.TREE, 0, new Date(5), new byte[] { 0 }));
  }

  @Test
  public void test_data() {
    assertNotNull(new TableRow(Table.DATA, 0, "someHash1", new byte[] { 0 }));
  }

  @Test
  public void test_iae_index() {
    try {
      new TableRow(Table.USER, "myUserName1");
      assertFalse(true);
    } catch (IllegalArgumentException exc) {
      assertEquals("Not enough row values for columns", exc.getMessage());
    }
  }

  @Test
  public void test_iae_String() {
    try {
      new TableRow(Table.USER, "myUserName1", 5);
      assertFalse(true);
    } catch (IllegalArgumentException exc) {
      assertTrue(exc.getMessage().endsWith("not of type String"));
    }
  }

  @Test
  public void test_iae_Int() {
    try {
      new TableRow(Table.TREE, "asdf", new Date(5), new byte[] { 0 });
      assertFalse(true);
    } catch (IllegalArgumentException exc) {
      assertTrue(exc.getMessage().endsWith("not of type Integer"));
    }
  }

  @Test
  public void test_iae_Date() {
    try {
      new TableRow(Table.TREE, 0, "asdf", new byte[] { 0 });
      assertFalse(true);
    } catch (IllegalArgumentException exc) {
      assertTrue(exc.getMessage().endsWith("not of type Date/Timestamp"));
    }
  }

  @Test
  public void test_iae_byteArray() {
    try {
      new TableRow(Table.TREE, 0, new Date(5), "asdf");
      assertFalse(true);
    } catch (IllegalArgumentException exc) {
      assertTrue(exc.getMessage().endsWith("not of type byte[]"));
    }
  }

}
