package org.vaultage.server.database;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vaultage.server.database.table.Table;
import org.vaultage.server.database.table.TableRow;

public class DatabaseWriterTest {

  private static Database db;
  private static DatabaseWriter dbWriter;

  @Before
  public void setUp() throws Exception {
    db = new Database("database/test/test", "sa", "", new File(
        "src/main/resources/db_vaultage_dev.sql"));
    db.start();
    dbWriter = new DatabaseWriter(db);
  }

  @After
  public void breakDown() throws Exception {
    db.shutdown();
    FileUtils.deleteDirectory(new File("database"));
  }

  @Test
  public void test_user() throws SQLException {
    TableRow row1 = new TableRow(Table.USER, "myUserName1", "someHash1");
    TableRow row2 = new TableRow(Table.USER, "myUserName2", "someHash2");
    assertEquals(1, dbWriter.write(row1));
    assertEquals(2, dbWriter.write(row2));
    List<Object[]> result = db.query(db.prepareStatement("select * from USER"));
    assertEquals(3, result.size());
    check(result.get(1), 1, "myUserName1", "someHash1");
    check(result.get(2), 2, "myUserName2", "someHash2");
  }

  @Test
  public void test_tree() throws SQLException {
    TableRow row1 = new TableRow(Table.TREE, 0, new Date(5), new byte[] { 0 });
    TableRow row2 = new TableRow(Table.TREE, 0, new Date(0), new byte[] { 0, 1 });
    TableRow row3 = new TableRow(Table.TREE, 0, new Date(1000), new byte[] { 0, 1, 2 });
    assertEquals(0, dbWriter.write(row1));
    assertEquals(1, dbWriter.write(row2));
    assertEquals(2, dbWriter.write(row3));
    List<Object[]> result = db.query(db.prepareStatement("select * from TREE"));
    assertEquals(3, result.size());
    check(result.get(0), 0, 0, new Date(5), new byte[] { 0 });
    check(result.get(1), 1, 0, new Date(0), new byte[] { 0, 1 });
    check(result.get(2), 2, 0, new Date(1000), new byte[] { 0, 1, 2 });
  }

  @Test
  public void test_data() throws SQLException {
    TableRow row1 = new TableRow(Table.DATA, 0, "someHash1", new byte[] { 0 });
    TableRow row2 = new TableRow(Table.DATA, 0, "someHash2", new byte[] { 0, 1 });
    TableRow row3 = new TableRow(Table.DATA, 0, "someHash3", new byte[] { 0, 1, 2 });
    assertEquals(0, dbWriter.write(row1));
    assertEquals(1, dbWriter.write(row2));
    assertEquals(2, dbWriter.write(row3));
    List<Object[]> result = db.query(db.prepareStatement("select * from DATA"));
    assertEquals(3, result.size());
    check(result.get(0), 0, 0, "someHash1", new byte[] { 0 });
    check(result.get(1), 1, 0, "someHash2", new byte[] { 0, 1 });
    check(result.get(2), 2, 0, "someHash3", new byte[] { 0, 1, 2 });
  }

  private void check(Object[] row, int id, Object... values) {
    int offset = 0;
    if (id >= 0) {
      assertEquals(id, row[0]);
      offset = 1;
    }
    for (int i = 0; i < values.length; i++) {
      if (values[i] instanceof byte[]) {
        Arrays.equals((byte[]) values[i], (byte[]) row[i + offset]);
      } else {
        assertEquals(values[i], row[i + offset]);
      }
    }
  }

}
