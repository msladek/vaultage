package org.vaultage.commons.tree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vaultage.commons.file.diff.Diff;
import org.vaultage.commons.file.diff.DiffType;
import org.vaultage.commons.file.meta.IMeta;
import org.vaultage.commons.file.meta.MetaFile;

import ch.marcsladek.commons.util.tree.TreeNode;

public class TreeBuilderTest {

  private final Path TEST = Paths.get("test");
  private final Path TEST1 = TEST.resolve("test1");
  private final Path TEST2 = TEST.resolve("test2");

  private final Path ROOT1 = TEST1.resolve("root");
  private final Path ROOT2 = TEST2.resolve("root");
  private final Path DIR1 = Paths.get("dir1");
  private final Path DIR2 = Paths.get("dir2");
  private final Path DIR1_DIR3 = DIR1.resolve("dir3");
  private final Path DIR1_FILE1 = DIR1.resolve("file1");
  private final Path DIR1_FILE2 = DIR1.resolve("file2");
  private final Path DIR2_FILE3 = DIR2.resolve("file3");
  private final Path DIR1_DIR3_FILE4 = DIR1_DIR3.resolve("file4");

  private final Path DIR2_FILENEW = DIR2.resolve("fileNew");
  private final Path DIR2_DIRNEW = DIR2.resolve("dirNew");

  @Before
  public void setUp() throws IOException {
    Files.createDirectories(ROOT1);
    Files.createDirectories(ROOT2);
  }

  @Test
  public void test_createTree() throws IOException {
    Path root = ROOT1;
    create(root);

    TreeNode<IMeta> rootNode = new MetaTreeBuilder().build(ROOT1);
    assertDetailNode(rootNode, root.getFileName(), root.getFileName(), null, 2, true,
        null);

    TreeNode<IMeta> dir1 = rootNode.getChild("dir1");
    assertDetailNode(dir1, root.getFileName().resolve(DIR1), DIR1.getFileName(),
        rootNode, 3, true, null);

    TreeNode<IMeta> dir2 = rootNode.getChild("dir2");
    assertDetailNode(dir2, root.getFileName().resolve(DIR2), DIR2.getFileName(),
        rootNode, 1, true, null);

    TreeNode<IMeta> dir3 = dir1.getChild(0);
    assertDetailNode(dir3, root.getFileName().resolve(DIR1_DIR3),
        DIR1_DIR3.getFileName(), dir1, 1, true, null);

    TreeNode<IMeta> file1 = dir1.getChild(1);
    assertDetailNode(file1, root.getFileName().resolve(DIR1_FILE1),
        DIR1_FILE1.getFileName(), dir1, 0, false, "3ddd583fd0c5a1e34ab7d45a8c89aa46");

    TreeNode<IMeta> file2 = dir1.getChild(2);
    assertDetailNode(file2, root.getFileName().resolve(DIR1_FILE2),
        DIR1_FILE2.getFileName(), dir1, 0, false, "e60ab3f70c52fa98cb97b3b87af4847d");

    TreeNode<IMeta> file3 = dir2.getChild(0);
    assertDetailNode(file3, root.getFileName().resolve(DIR2_FILE3),
        DIR2_FILE3.getFileName(), dir2, 0, false, "a9937e4438b909ec438d89d7545d970e");

    TreeNode<IMeta> file4 = dir3.getChild(0);
    assertDetailNode(file4, root.getFileName().resolve(DIR1_DIR3_FILE4),
        DIR1_DIR3_FILE4.getFileName(), dir3, 0, false, "3aefa245db43192d295576fda28240fc");
  }

  @Test
  public void testGetTreeDiff_equal() throws IOException {
    create(ROOT1);
    create(ROOT2);

    TreeNode<Diff> diff = new DiffTreeBuilder().build(new MetaTreeBuilder().build(ROOT1),
        new MetaTreeBuilder().build(ROOT2));
    assertNull(diff);
  }

  @Test
  public void testGetTreeDiff_deleted_all() throws IOException {
    create(ROOT1);

    TreeNode<Diff> diff = new DiffTreeBuilder().build(new MetaTreeBuilder().build(ROOT1),
        new MetaTreeBuilder().build(ROOT2));
    assertDiffNode(diff, "root", "root", null, 2, DiffType.UNCHANGED, null, null, true,
        true);

    TreeNode<Diff> dir1 = diff.getChild("dir1");
    assertDiffNode(dir1, "root/dir1", "dir1", diff, 0, DiffType.DELETED, null, null,
        true, false);

    TreeNode<Diff> dir2 = diff.getChild("dir2");
    assertDiffNode(dir2, "root/dir2", "dir2", diff, 0, DiffType.DELETED, null, null,
        true, false);
  }

  /*
   * � root (unchanged) � dir1 (unchanged) * dir3 (modified) * file1 (modified) * file2
   * (deleted) � dir2 (unchanged) � dirNew (new) * fileNew (new)
   */
  @Test
  public void testGetTreeDiff_diff() throws IOException {
    create(ROOT1);
    createDiff(ROOT2);
    TreeNode<Diff> diff = new DiffTreeBuilder().build(new MetaTreeBuilder().build(ROOT1),
        new MetaTreeBuilder().build(ROOT2));
    assertDiffNode(diff, "root", "root", null, 2, DiffType.UNCHANGED, null, null, true,
        true);

    TreeNode<Diff> dir1 = diff.getChild("dir1");
    assertDiffNode(dir1, "root/dir1", "dir1", diff, 3, DiffType.UNCHANGED, null, null,
        true, true);

    TreeNode<Diff> dir3 = dir1.getChild("dir3");
    assertDiffNode(dir3, "root/dir1/dir3", "dir3", dir1, 0, DiffType.MODIFIED, null,
        "97e975eb518923acfd41fc92b4855bb7", true, false);

    TreeNode<Diff> file1 = dir1.getChild("file1");
    assertDiffNode(file1, "root/dir1/file1", "file1", dir1, 0, DiffType.MODIFIED,
        "3ddd583fd0c5a1e34ab7d45a8c89aa46", "fdebd72466180ec4fff02e799fae3d25", false,
        false);

    TreeNode<Diff> file2 = dir1.getChild("file2");
    assertDiffNode(file2, "root/dir1/file2", "file2", dir1, 0, DiffType.DELETED,
        "e60ab3f70c52fa98cb97b3b87af4847d", null, false, false);

    TreeNode<Diff> dir2 = diff.getChild("dir2");
    assertDiffNode(dir2, "root/dir2", "dir2", diff, 2, DiffType.UNCHANGED, null, null,
        true, true);

    TreeNode<Diff> dirNew = dir2.getChild("dirNew");
    assertDiffNode(dirNew, "root/dir2/dirNew", "dirNew", dir2, 0, DiffType.NEW, null,
        null, false, true);

    TreeNode<Diff> fileNew = dir2.getChild("fileNew");
    assertDiffNode(fileNew, "root/dir2/fileNew", "fileNew", dir2, 0, DiffType.NEW, null,
        "6f94bd2bdfbdd3e9d529c77c92db85ab", false, false);
  }

  @After
  public void breakDown() throws IOException {
    FileUtils.deleteDirectory(TEST.toFile());
  }

  private void assertDiffNode(TreeNode<Diff> diffNode, String path, String name,
      TreeNode<Diff> parent, int childrenSize, DiffType diffType, String hashOld,
      String hashNew, boolean wasDirectory, boolean isDirectory) {

    assertNotNull(diffNode);
    assertEquals(path, diffNode.getPath());
    assertEquals(name, diffNode.getName());
    assertSame(parent, diffNode.getParent());
    assertEquals(childrenSize, diffNode.getChildrenSize());

    Diff diff = diffNode.getElement();
    assertEquals(diffType, diff.getDiffType());

    IMeta metaOld = diff.getMetaOld();
    if (metaOld != null) {
      assertEquals(TEST1.resolve(path).toString(), metaOld.getPath());
      assertEquals(hashOld, metaOld.getHash());
      assertEquals(wasDirectory, metaOld.isFolder());
    } else {
      assertEquals(hashOld, null);
      assertEquals(wasDirectory, false);
    }

    IMeta metaNew = diff.getMetaNew();
    if (metaNew != null) {
      assertEquals(TEST2.resolve(path).toString(), metaNew.getPath());
      assertEquals(hashNew, metaNew.getHash());
      assertEquals(isDirectory, metaNew.isFolder());
    } else {
      assertEquals(hashNew, null);
      assertEquals(isDirectory, false);
    }
  }

  private void assertDetailNode(TreeNode<IMeta> node, Path path, Path name,
      TreeNode<IMeta> parent, int childrenSize, boolean isDirectory, String hash) {
    assertNotNull(node);
    assertEquals(path, Paths.get(node.getPath()));
    assertEquals(name, Paths.get(node.getName()));
    assertSame(parent, node.getParent());
    assertEquals(childrenSize, node.getChildrenSize());
    assertEquals(isDirectory, node.getElement().isFolder());
    assertEquals(hash, getHash(node.getElement()));
  }

  private static String getHash(IMeta metaData) {
    if (!metaData.isFolder()) {
      return ((MetaFile) metaData).getHash();
    } else {
      return null;
    }
  }

  /*
   * � root � dir1 � dir3 * file4 * file1 * file2 � dir2 * file3
   */
  private void create(Path root) throws IOException {
    Files.createDirectories(root.resolve(DIR1));
    Files.createDirectories(root.resolve(DIR2));
    Files.createDirectories(root.resolve(DIR1_DIR3));
    createFile(root.resolve(DIR1_FILE1), "This is file1");
    createFile(root.resolve(DIR1_FILE2), "This is file2");
    createFile(root.resolve(DIR2_FILE3), "This is file3");
    createFile(root.resolve(DIR1_DIR3_FILE4), "This is file4");
  }

  /*
   * DELETED FILE: dir1/file2 NEW FILE: dir2/fileNew NEW FOLDER: dir2/dirNew MODIFIED
   * FILE: dir1/file1 MODIFIED FOLDER: dir1/dir3 � root � dir1 * dir3 * file1 (mod) � dir2
   * � dirNew * file3 * fileNew
   */
  private void createDiff(Path root) throws IOException {
    Files.createDirectories(root.resolve(DIR1));
    Files.createDirectories(root.resolve(DIR2));
    Files.createDirectories(root.resolve(DIR2_DIRNEW));
    createFile(root.resolve(DIR1_FILE1), "This is file1 but with new content!");
    createFile(root.resolve(DIR2_FILE3), "This is file3");
    createFile(root.resolve(DIR1_DIR3), "This is no longer a dir");
    createFile(root.resolve(DIR2_FILENEW), "This is fileNew");
  }

  private void createFile(Path path, String content) throws IOException {
    FileWriter fw = new FileWriter(path.toFile());
    try {
      fw.write(content);
    } finally {
      IOUtils.closeQuietly(fw);
    }
  }
}
