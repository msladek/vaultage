package org.vaultage.commons.file;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.IOUtils;

public class FileCreator {

  /**
   * creates files
   * 
   * @param dir
   *          directory of the created files
   * @param amount
   *          amount of files created
   * @param size
   *          size per file in KB
   * @throws IOException
   */
  public static void createFiles(Path dir, int amount, int size) throws IOException {
    String name = "file";
    for (int i = 1; i <= amount; i++) {
      createFile(dir, name + "-" + i, size);
    }
  }

  /**
   * creates a file
   * 
   * @param dir
   *          directory of the created file
   * @param name
   *          name of the file created
   * @param size
   *          size of the file in KB
   * @throws IOException
   */
  public static void createFile(Path dir, String name, int size) throws IOException {
    createFile(dir, name, new byte[1024 * size]);
  }

  /**
   * creates a file
   * 
   * @param dir
   *          directory of the created file
   * @param name
   *          name of the file created
   * @param content
   *          conetnt of the file
   * 
   * @throws IOException
   */
  public static void createFile(Path dir, String name, byte[] content) throws IOException {
    Files.createDirectories(dir);
    OutputStream out = null;
    try {
      Path file = dir.resolve(name);
      out = new BufferedOutputStream(Files.newOutputStream(file));
      out.write(content);
      System.out.println("Created: " + file.getFileName() + " Size: "
          + (content.length / 1024) + "KB");
    } finally {
      IOUtils.closeQuietly(out);
    }
  }

  public static void main(String[] args) throws IOException {
    String userHome = System.getProperty("user.home");
    Path dir = Paths.get(userHome, "vaultage");
    FileCreator.createFiles(dir, 10, 1024 * 100);
  }
}
