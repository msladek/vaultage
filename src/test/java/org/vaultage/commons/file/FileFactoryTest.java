package org.vaultage.commons.file;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;
import org.vaultage.commons.file.meta.IMeta;
import org.vaultage.commons.file.meta.MetaFile;

public class FileFactoryTest {

  @Test
  public void testGenerateHash() throws IOException, NoSuchAlgorithmException {
    Path dir = Paths.get("test");
    String name = "asdf";
    String content = "muhu ;)";
    FileCreator.createFile(dir, name, content.getBytes());
    IMeta metaData = FileFactory.getMeta(dir.resolve(name));
    String hash = Hex.encodeHexString(MessageDigest.getInstance("MD5").digest(
        content.getBytes()));
    assertEquals(hash, ((MetaFile) metaData).getHash());
    Files.delete(dir.resolve(name));
    Files.delete(dir);
  }

}
