package org.vaultage.commons.file;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

import org.vaultage.commons.file.meta.MetaFile;

public final class FileData implements Serializable {

  private static final long serialVersionUID = 201302051242L;

  private final MetaFile metaData;
  private final byte[] data;

  FileData(MetaFile metaData, byte[] data) {
    this.metaData = Objects.requireNonNull(metaData);
    this.data = Objects.requireNonNull(data);
    if (metaData.getSize() != data.length) {
      throw new IllegalArgumentException("Data doesn't have same size as defined in "
          + "MetaData: " + data.length + " - " + metaData.getSize());
    }
  }

  public MetaFile getFileMetaData() {
    return metaData;
  }

  public byte[] getData() {
    return Arrays.copyOf(data, data.length);
  }

  @Override
  public int hashCode() {
    return Objects.hash(metaData.hashCode(), Arrays.hashCode(data));
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof FileData) {
      FileData other = (FileData) obj;
      return Objects.equals(metaData, other.metaData) && Arrays.equals(data, other.data);
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "FileData [path=" + metaData.getPath() + ", size=" + metaData.getSize()
        + ", changeDate=" + metaData.getChangeDate() + ", hash=" + metaData.getHash()
        + "]";
  }

}
