package org.vaultage.commons.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.vaultage.commons.file.meta.IMeta;
import org.vaultage.commons.file.meta.MetaFile;
import org.vaultage.commons.file.meta.MetaFolder;

import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public final class FileFactory {

  private static final Logger LOGGER = LogFactory.getLogger(FileFactory.class);

  public static final IMeta getMeta(Path path) throws IOException {
    LOGGER.trace("Getting meta for path '" + path + "'");
    long changeDate = Files.getLastModifiedTime(path).toMillis();
    if (Files.isDirectory(path)) {
      return new MetaFolder(path.toString(), changeDate);
    } else {
      return new MetaFile(path.toString(), changeDate, generateHash(path),
          Files.size(path));
    }
  }

  private static String generateHash(Path path) throws IOException {
    FileInputStream fis = null;
    try {
      fis = new FileInputStream(path.toFile());
      return DigestUtils.md5Hex(fis);
    } finally {
      IOUtils.closeQuietly(fis);
    }
  }

  public static final FileData getData(Path path) throws IOException {
    IMeta metaData = getMeta(path);
    if (!metaData.isFolder()) {
      return getData((MetaFile) metaData);
    } else {
      throw new IOException("Unable to get data since given path '" + path
          + "' is a folder");
    }
  }

  public static final FileData getData(MetaFile metaData) throws IOException {
    return new FileData(metaData, Files.readAllBytes(Paths.get(metaData.getPath())));
  }

}
