package org.vaultage.commons.file;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import org.vaultage.commons.file.diff.DiffType;

public final class FileDiff {

  private final DiffType diffType;
  private final String hash;
  private final String path;
  private final boolean wasDirectory;
  private final boolean isDirectory;

  FileDiff(DiffType diffType, String hash, Path path, boolean wasDirectory,
      boolean isDirectory) {
    this.diffType = diffType;
    this.hash = hash;
    this.path = path.toString();
    this.wasDirectory = wasDirectory;
    this.isDirectory = isDirectory;
  }

  public DiffType getDiffType() {
    return diffType;
  }

  public String getHash() {
    return hash;
  }

  public Path getPath() {
    return Paths.get(path);
  }

  public boolean wasDirectory() {
    return wasDirectory;
  }

  public boolean isDirectory() {
    return isDirectory;
  }

  @Override
  public int hashCode() {
    return Objects.hash(diffType, hash, path);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }
    FileDiff other = (FileDiff) obj;
    return Objects.equals(diffType, other.diffType) && Objects.equals(hash, other.hash)
        && Objects.equals(path, other.path);
  }

  @Override
  public String toString() {
    return "FileDiff [diffType=" + diffType + ", hash=" + hash + ", path=" + path + "]";
  }

}
