package org.vaultage.commons.file.diff;

public enum DiffType {

  UNCHANGED, MODIFIED, DELETED, NEW;

}
