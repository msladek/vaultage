package org.vaultage.commons.file.diff;

import java.io.Serializable;
import java.util.Objects;

import org.vaultage.commons.file.meta.IMeta;

public class Diff implements Serializable {

  private static final long serialVersionUID = 201302070149L;

  private final DiffType diffType;
  private final IMeta metaOld;
  private final IMeta metaNew;

  Diff(DiffType diffType, IMeta metaOld, IMeta metaNew) {
    this.diffType = Objects.requireNonNull(diffType);
    this.metaOld = metaOld;
    this.metaNew = metaNew;
  }

  public DiffType getDiffType() {
    return diffType;
  }

  public IMeta getMetaOld() {
    return metaOld;
  }

  public IMeta getMetaNew() {
    return metaNew;
  }

  @Override
  public int hashCode() {
    return Objects.hash(diffType, metaOld, metaNew);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Diff) {
      Diff other = (Diff) obj;
      return Objects.equals(diffType, other.diffType)
          && Objects.equals(metaOld, other.metaOld)
          && Objects.equals(metaNew, other.metaNew);
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "Diff [diffType=" + diffType + ", metaOld=" + metaOld + ", metaNew=" + metaNew
        + "]";
  }

}
