package org.vaultage.commons.file.diff;

import java.util.Objects;

import org.vaultage.commons.file.meta.IMeta;

import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class DiffFactory {

  private static final Logger LOGGER = LogFactory.getLogger(DiffFactory.class);

  public static final Diff getDiff(IMeta metaOld, IMeta metaNew) {
    DiffType diffType = determineDiffType(metaOld, metaNew);
    LOGGER.trace("DiffType determined is '" + diffType + "' for metaOld '" + metaOld
        + "' and metaNew '" + metaNew + "'");
    return new Diff(diffType, metaOld, metaNew);
  }

  private static DiffType determineDiffType(IMeta metaOld, IMeta metaNew) {
    if (isIllegal(metaOld, metaNew)) {
      throw new IllegalArgumentException("Neither old nor new metaData");
    } else if (isUnchanged(metaOld, metaNew)) {
      return DiffType.UNCHANGED;
    } else if (isModified(metaOld, metaNew)) {
      return DiffType.MODIFIED;
    } else if (isDeleted(metaOld, metaNew)) {
      return DiffType.DELETED;
    } else if (isNew(metaOld, metaNew)) {
      return DiffType.NEW;
    } else {
      return null;
    }
  }

  private static boolean isIllegal(IMeta metaOld, IMeta metaNew) {
    return (metaOld == null) && (metaNew == null);
  }

  private static boolean isNew(IMeta metaOld, IMeta metaNew) {
    return (metaOld == null) && (metaNew != null);
  }

  private static boolean isDeleted(IMeta metaOld, IMeta metaNew) {
    return (metaOld != null) && (metaNew == null);
  }

  private static boolean isModified(IMeta metaOld, IMeta metaNew) {
    return (metaOld != null) && (metaNew != null)
        && !Objects.equals(metaOld.getHash(), metaNew.getHash());
  }

  private static boolean isUnchanged(IMeta metaOld, IMeta metaNew) {
    return (metaOld != null) && (metaNew != null)
        && Objects.equals(metaOld.getHash(), metaNew.getHash());
  }

}
