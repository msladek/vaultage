package org.vaultage.commons.file.meta;

import java.io.Serializable;

public interface IMeta extends Serializable {

  public String getPath();

  public long getChangeDate();

  public String getHash();

  public boolean isFolder();

}
