package org.vaultage.commons.file.meta;

import java.util.Objects;

public class MetaFolder implements IMeta {

  private static final long serialVersionUID = 201302051230L;

  private final String path;
  private final long changeDate;

  public MetaFolder(String path, long changeDate) {
    this.path = Objects.requireNonNull(path);
    this.changeDate = changeDate;
  }

  @Override
  public final String getPath() {
    return path;
  }

  @Override
  public final long getChangeDate() {
    return changeDate;
  }

  @Override
  public String getHash() {
    return null;
  }

  @Override
  public boolean isFolder() {
    return !(this instanceof MetaFile);
  }

  @Override
  public int hashCode() {
    return Objects.hash(path, changeDate);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof MetaFolder) {
      MetaFolder other = (MetaFolder) obj;
      return Objects.equals(path, other.path)
          && Objects.equals(changeDate, other.changeDate);
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "MetaFolder [path=" + path + ", changeDate=" + changeDate + "]";
  }

}
