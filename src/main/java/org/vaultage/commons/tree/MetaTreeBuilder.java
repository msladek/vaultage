package org.vaultage.commons.tree;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.vaultage.commons.file.FileFactory;
import org.vaultage.commons.file.meta.IMeta;

import ch.marcsladek.commons.util.tree.TreeNode;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class MetaTreeBuilder {

  private static final Logger LOGGER = LogFactory.getLogger(MetaTreeBuilder.class);

  /**
   * Creates a Tree containing TreeNodes with {@link IMeta} objects representing the given
   * path for the root folder.
   * 
   * @param pathRoot
   *          path of root folder which should be represented. May not be null.
   * @return the root node of the tree
   * @throws IOException
   *           when unable to read out files
   */
  public TreeNode<IMeta> build(Path pathRoot) throws IOException {
    LOGGER.debug("Creating TreeMeta for '" + pathRoot + "'");
    MetaTreeBuilderVisitor visitor = new MetaTreeBuilderVisitor();
    Files.walkFileTree(pathRoot, visitor);
    LOGGER.trace("TreeMeta build finished.");
    return visitor.getRoot();
  }

}

class MetaTreeBuilderVisitor extends SimpleFileVisitor<Path> {

  private static final Logger LOGGER = LogFactory.getLogger(MetaTreeBuilderVisitor.class);

  private TreeNode<IMeta> root;
  private TreeNode<IMeta> currentDirNode;

  TreeNode<IMeta> getRoot() {
    return root;
  }

  @Override
  public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
      throws IOException {
    TreeNode<IMeta> dirNode = getNewTreeNode(dir);
    if (root == null) {
      LOGGER.trace("Setting root to '" + dirNode.toStringSimple() + "'");
      root = dirNode;
    } else {
      LOGGER.trace("Adding DIR '" + dirNode.toStringSimple() + "'");
      currentDirNode.addChild(dirNode);
    }
    currentDirNode = dirNode;
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
    LOGGER.trace("Finished DIR '" + currentDirNode.toStringSimple() + "'");
    currentDirNode = currentDirNode.getParent();
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
      throws IOException {
    try {
      TreeNode<IMeta> fileNode = getNewTreeNode(file);
      LOGGER.trace("Adding FILE '" + fileNode.toStringSimple() + "'");
      currentDirNode.addChild(fileNode);
    } catch (IOException exc) {
      LOGGER.warn("Unable to add FILE '" + file + "'");
    }
    return FileVisitResult.CONTINUE;
  }

  private TreeNode<IMeta> getNewTreeNode(Path path) throws IOException {
    return new TreeNode<IMeta>(path.getFileName().toString(), FileFactory.getMeta(path));
  }

}
