package org.vaultage.commons.tree;

import java.util.List;

import org.vaultage.commons.file.diff.Diff;
import org.vaultage.commons.file.diff.DiffFactory;
import org.vaultage.commons.file.diff.DiffType;
import org.vaultage.commons.file.meta.IMeta;

import ch.marcsladek.commons.util.tree.TreeNode;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class DiffTreeBuilder {

  private static final Logger LOGGER = LogFactory.getLogger(DiffTreeBuilder.class);

  /**
   * Creates a Tree containing {@link TreeNode}s with {@link IDiff} objects representing
   * the differences between the two given trees. The given root nodes must have the same
   * name.
   * 
   * @param treeOld
   *          The old tree structure. May not be null.
   * @param treeNew
   *          The new tree structure. May not be null.
   * @return tree root node of the diff tree
   */
  public TreeNode<Diff> build(TreeNode<IMeta> treeOld, TreeNode<IMeta> treeNew) {
    LOGGER.debug("Creating TreeDiff for '" + treeOld.toStringSimple() + "' and '"
        + treeNew.toStringSimple() + "'");
    checkRoots(treeOld, treeNew);
    TreeNode<Diff> tree = createTreeDiff(treeOld, treeNew);
    LOGGER.trace("TreeMeta build finished.");
    return tree;
  }

  /*
   * verifies root nodes to guarantee correct TreeDiff
   */
  private void checkRoots(TreeNode<IMeta> node1, TreeNode<IMeta> node2) {
    String msg = null;
    if ((node1 == null) || (node2 == null)) {
      msg = "Root node is null";
    } else if (!node1.isRoot() || !node2.isRoot()) {
      msg = "Root node is not root";
    } else if (!node1.getName().equals(node2.getName())) {
      msg = "Root nodes have different name";
    } else if (!node1.getElement().isFolder() || !node2.getElement().isFolder()) {
      msg = "Root node contains not a folder element";
    }
    if (msg != null) {
      throw new IllegalArgumentException(msg + " in getTreeDiff: "
          + node1.toStringSimple() + ", " + node2.toStringSimple());
    }
  }

  /*
   * recursive function for deep tree creation
   */
  private TreeNode<Diff> createTreeDiff(TreeNode<IMeta> nodeOld, TreeNode<IMeta> nodeNew) {
    TreeNode<Diff> ret = getFileDiffNode(nodeOld, nodeNew);
    if (hasToAddChildren(ret)) {
      List<TreeNode<IMeta>> childrenNew = addChildDiffFromOld(nodeOld, nodeNew, ret);
      addChildDiffsFromNew(childrenNew, ret);
    }
    return getFilteredNode(ret);
  }

  /*
   * children only have to be added if it is a non-deleted folder
   */
  private boolean hasToAddChildren(TreeNode<Diff> node) {
    if (node != null) {
      Diff diff = node.getElement();
      return (diff.getDiffType() != DiffType.DELETED) && diff.getMetaNew().isFolder();
    } else {
      return false;
    }
  }

  /*
   * While adding children from childrenOld, childrenNew are already being sorted out and
   * returned afterwards for faster calculation.
   */
  private List<TreeNode<IMeta>> addChildDiffFromOld(TreeNode<IMeta> nodeOld,
      TreeNode<IMeta> nodeNew, TreeNode<Diff> ret) {
    List<TreeNode<IMeta>> childrenNew = nodeNew.getChildren();
    for (TreeNode<IMeta> childOld : nodeOld.getChildren()) {
      TreeNode<IMeta> childNew = removeFromList(childOld.getName(), childrenNew);
      TreeNode<Diff> childDiff = createTreeDiff(childOld, childNew);
      if (childDiff != null) {
        ret.addChild(childDiff);
      }
    }
    return childrenNew;
  }

  private TreeNode<IMeta> removeFromList(String name, List<TreeNode<IMeta>> childrenNew) {
    for (int i = 0; i < childrenNew.size(); i++) {
      if (childrenNew.get(i).getName().equals(name)) {
        return childrenNew.remove(i--);
      }
    }
    return null;
  }

  /*
   * These children always have type NEW (childOld parameter is null)
   */
  private void addChildDiffsFromNew(List<TreeNode<IMeta>> childrenNew, TreeNode<Diff> ret) {
    for (TreeNode<IMeta> childNew : childrenNew) {
      ret.addChild(getFileDiffNode(null, childNew));
    }
  }

  private TreeNode<Diff> getFileDiffNode(TreeNode<IMeta> nodeOld, TreeNode<IMeta> nodeNew) {
    String name = (nodeNew != null ? nodeNew.getName() : (nodeOld != null ? nodeOld
        .getName() : ""));
    IMeta metaOld = (nodeOld != null ? nodeOld.getElement() : null);
    IMeta metaNew = (nodeNew != null ? nodeNew.getElement() : null);
    return new TreeNode<Diff>(name, DiffFactory.getDiff(metaOld, metaNew));
  }

  /*
   * Unnecessary nodes can be generated (e.g. File with type UNCHANGED). They should not
   * be returned.
   */
  private TreeNode<Diff> getFilteredNode(TreeNode<Diff> node) {
    Diff diff = (node != null ? node.getElement() : null);
    if ((diff != null) && (diff.getDiffType() == DiffType.UNCHANGED)
        && (!diff.getMetaNew().isFolder() || (node.getChildrenSize() == 0))) {
      return null;
    } else {
      return node;
    }
  }

}
