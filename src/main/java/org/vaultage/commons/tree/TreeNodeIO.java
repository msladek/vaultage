package org.vaultage.commons.tree;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Path;

import org.apache.commons.io.IOUtils;

import ch.marcsladek.commons.util.tree.TreeNode;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class TreeNodeIO<T extends Serializable> {

  private static final Logger LOGGER = LogFactory.getLogger(TreeNodeIO.class);

  public TreeNode<T> read(Path path) throws IOException, ClassNotFoundException {
    ObjectInputStream ois = null;
    try {
      ois = new ObjectInputStream(new FileInputStream(path.toFile()));
      Object obj = ois.readObject();
      TreeNode<T> tree = cast(obj);
      LOGGER.debug("'" + tree.toStringSimple() + "' successfully read from '" + path
          + "'");
      return tree;
    } finally {
      IOUtils.closeQuietly(ois);
    }
  }

  @SuppressWarnings("unchecked")
  private TreeNode<T> cast(Object obj) throws ClassNotFoundException {
    if (obj instanceof TreeNode) {
      return (TreeNode<T>) obj;
    } else {
      throw new ClassNotFoundException("Unable to cast read object to TreeNode");
    }
  }

  public void write(Path path, TreeNode<T> tree) throws IOException {
    ObjectOutputStream oos = null;
    try {
      oos = new ObjectOutputStream(new FileOutputStream(path.toFile()));
      oos.writeObject(tree);
      LOGGER.debug("'" + tree.toStringSimple() + "' successfully written to '" + path
          + "'");
    } finally {
      IOUtils.closeQuietly(oos);
    }
  }

}
