package org.vaultage.commons.auth;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.codec.digest.DigestUtils;

public final class Auth implements Serializable {

  private static final long serialVersionUID = 201302010213L;

  private final String username;
  private final String hash;

  public Auth(String username, String password) {
    Objects.requireNonNull(username);
    Objects.requireNonNull(password);
    this.username = username;
    this.hash = DigestUtils.sha512Hex(password);
  }

  public String getUsername() {
    return username;
  }

  public String getHash() {
    return hash;
  }

  @Override
  public int hashCode() {
    return Objects.hash(username, getHash());
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Auth) {
      Auth other = (Auth) obj;
      return Objects.equals(username, other.username)
          && Objects.equals(getHash(), other.getHash());
    } else {
      return false;
    }
  }

}
