package org.vaultage.client.comm;

import java.io.IOException;
import java.net.Socket;

import ch.marcsladek.commons.concurrent.startable.Startable;
import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionException;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class ServerConnection extends Connection {

  private static final Logger LOGGER = LogFactory.getLogger(ServerConnection.class);

  private Startable startable;

  ServerConnection(Socket socket) throws IOException {
    super(socket);
  }

  void setStartable(Startable startable) {
    this.startable = startable;
  }

  @Override
  protected void process(Object obj) {
    LOGGER.debug("Object received: " + obj);
  }

  @Override
  protected void finish(boolean closedRemotely, ConnectionException closeException) {
    LOGGER.trace("Close Exception for " + (closedRemotely ? "remote" : "non-remote")
        + " close", closeException);
    if ((startable != null) && closedRemotely) {
      LOGGER.debug("Shutdown invoked due to remote close");
      startable.shutdown();
    }
  }
}
