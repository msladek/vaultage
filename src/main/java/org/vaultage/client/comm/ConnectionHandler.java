package org.vaultage.client.comm;

import java.io.IOException;

import org.vaultage.commons.auth.Auth;
import org.vaultage.commons.file.meta.IMeta;

import ch.marcsladek.commons.concurrent.callable.Callable;
import ch.marcsladek.commons.concurrent.startable.Startable;
import ch.marcsladek.commons.util.tree.TreeNode;
import ch.marcsladek.jrtnp.client.Client;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class ConnectionHandler extends Client implements Callable<Void, TreeNode<IMeta>>,
    Startable {

  private static final Logger LOGGER = LogFactory.getLogger(ConnectionHandler.class);

  public ConnectionHandler(String host, int port, String username, String password)
      throws ReflectiveOperationException, IOException {
    super(host, port, false, ServerConnectionFactory.class);
    authenticate(username, password);
  }

  private void authenticate(String username, String password) throws IOException {
    Auth auth = new Auth(username, password);
    LOGGER.info("Trying to authenticate as '" + username + "'");
    server.send(auth);
  }

  public void setStartable(Startable startable) {
    ((ServerConnection) server).setStartable(startable);
  }

  @Override
  public void started() {
    LOGGER.info("ConnectionHandler started for " + server.toString());
  }

  @Override
  public void shuttedDown() {
    LOGGER.info("ConnectionHandler shutdown for " + server.toString());
  }

  @Override
  public Void call(TreeNode<IMeta> treeNode) {
    try {
      LOGGER.trace("Sending '" + treeNode.toStringSimple() + "' to Server");
      server.send(treeNode);
    } catch (IOException exc) {
      LOGGER.error("Error while sending occured", exc);
    }
    return null;
  }

  @Override
  public boolean isRunning() {
    return server.isListening();
  }

  @Override
  public boolean isTerminated() {
    return !server.isListening();
  }
}
