package org.vaultage.client.fileHandler;

import java.io.IOException;

import org.vaultage.client.Settings;
import org.vaultage.commons.file.meta.IMeta;
import org.vaultage.commons.tree.MetaTreeBuilder;
import org.vaultage.commons.tree.TreeNodeIO;

import ch.marcsladek.commons.concurrent.callable.Callable;
import ch.marcsladek.commons.util.tree.TreeNode;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class DirectoryChangeHandler {

  private static final Logger LOGGER = LogFactory.getLogger(DirectoryChangeHandler.class);

  private static final String treeFile = "workdir.tree";

  private final Settings settings;
  private final TreeNodeIO<IMeta> treeNodeIO;
  private final Callable<Void, TreeNode<IMeta>> treeNodeCallable;

  private MetaTreeBuilder metaTreeBuilder;

  public DirectoryChangeHandler(Settings settings,
      Callable<Void, TreeNode<IMeta>> treeNodeCallable) {
    this.settings = settings;
    this.treeNodeIO = new TreeNodeIO<IMeta>();
    this.treeNodeCallable = treeNodeCallable;
  }

  public boolean changed() {
    LOGGER.debug("Directory change invoked");
    try {
      TreeNode<IMeta> tree = getMetaTreeBuilder().build(settings.getWorkingDir());
      treeNodeIO.write(settings.getSettingsDir().resolve(treeFile), tree);
      treeNodeCallable.call(tree);
      LOGGER.debug("Calling callable with argument '" + tree.toStringSimple() + "'");
      return true;
    } catch (IOException exc) {
      LOGGER.warn("Exception while building or writing tree", exc);
      return false;
    }
  }

  private MetaTreeBuilder getMetaTreeBuilder() {
    if (metaTreeBuilder == null) {
      metaTreeBuilder = new MetaTreeBuilder();
    }
    return metaTreeBuilder;
  }

}
