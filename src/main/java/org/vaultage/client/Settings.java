package org.vaultage.client;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import ch.marcsladek.commons.util.properties.Properties;
import ch.marcsladek.commons.util.properties.Property;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class Settings {

  private static final Logger LOGGER = LogFactory.getLogger(Settings.class);

  private static final String USER_HOME = System.getProperty("user.home");
  private static final Path SETTINGS_DIR = Paths.get(USER_HOME, ".vaultage");
  private static final Path SETTINGS = SETTINGS_DIR.resolve("vaultage.properties");

  private static final Property<String> PROP_HOST = new Property<String>("host",
      "localhost");
  private static final Property<Integer> PROP_PORT = new Property<Integer>("port", 12963);
  private static final Property<String> PROP_WORKING_DIR = new Property<String>(
      "working-directory", Paths.get(USER_HOME, "vaultage").toString());
  private static final Property<String> PROP_LOGGING_DIR = new Property<String>(
      "logging-directory", SETTINGS_DIR.resolve("logs").toString());
  private static final Property<Integer> PROP_SYNC_INTERVAL = new Property<Integer>(
      "sync-interval", 30);

  private static List<Property<? extends Object>> getProperties() {
    List<Property<? extends Object>> list = new ArrayList<Property<? extends Object>>();
    list.add(PROP_HOST);
    list.add(PROP_PORT);
    list.add(PROP_WORKING_DIR);
    list.add(PROP_LOGGING_DIR);
    list.add(PROP_SYNC_INTERVAL);
    return list;
  }

  private final Properties properties;

  Settings() throws IOException, IllegalArgumentException {
    checkAndCreateDir(SETTINGS_DIR);
    LOGGER.info("Settings directory: " + SETTINGS_DIR);
    properties = new Properties(SETTINGS, getProperties());
    checkAndCreateDir(getWorkingDir());
    checkAndCreateDir(getLoggingDir());
  }

  private void checkAndCreateDir(Path dir) throws IOException {
    if (!Files.exists(dir)) {
      Files.createDirectories(dir);
      LOGGER.debug("Directory " + dir + " has been created.");
    } else if (!Files.isDirectory(dir)) {
      throw new IOException("Path is expected to be a Directory, but is in fact a File: "
          + dir);
    }
  }

  public Path getSettingsDir() {
    return SETTINGS_DIR;
  }

  public String getHost() {
    return (String) properties.getPropertyValue(PROP_HOST);
  }

  public int getPort() {
    return (Integer) properties.getPropertyValue(PROP_PORT);
  }

  public Path getWorkingDir() {
    return Paths.get((String) properties.getPropertyValue(PROP_WORKING_DIR));
  }

  public Path getLoggingDir() {
    return Paths.get((String) properties.getPropertyValue(PROP_LOGGING_DIR));
  }

  public int getSyncInterval() {
    return (Integer) properties.getPropertyValue(PROP_SYNC_INTERVAL);
  }

}
