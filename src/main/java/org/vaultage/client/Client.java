package org.vaultage.client;

import java.io.IOException;

import org.vaultage.client.comm.ConnectionHandler;
import org.vaultage.client.fileHandler.DirectoryChangeHandler;
import org.vaultage.client.fileHandler.DirectoryListener;
import org.vaultage.commons.file.meta.IMeta;

import ch.marcsladek.commons.concurrent.callable.Callable;
import ch.marcsladek.commons.concurrent.startable.Startable;
import ch.marcsladek.commons.concurrent.startable.Startup;
import ch.marcsladek.commons.io.ConsoleReader;
import ch.marcsladek.commons.util.tree.TreeNode;
import ch.marcsladek.yajlf.ErrorLevel;
import ch.marcsladek.yajlf.LogFactory;

public class Client {

  public static final boolean DEBUG = true;
  private static final String logFile = "log.txt";

  private static Startable startable;

  public static void main(String[] args) throws Exception {

    CommandLineArguments arguments = new CommandLineArguments(args);

    Settings settings = new Settings();
    setLog(settings);

    ConsoleReader consoleReader = getConsoleReader();
    ConnectionHandler connHandler = getConnectionHandler(settings, arguments);
    DirectoryListener listener = getDirectoryListener(settings, connHandler);

    startable = new Startup(connHandler, listener, consoleReader);
    connHandler.setStartable(startable);
    startable.start();
  }

  private static void setLog(Settings settings) throws IOException {
    LogFactory.setLog(settings.getLoggingDir().resolve(logFile).toString(),
        Client.DEBUG ? ErrorLevel.TRACE : ErrorLevel.INFO);
  }

  private static ConsoleReader getConsoleReader() {
    return new ConsoleReader() {
      @Override
      protected void process(String line) {
        if (line.toLowerCase().contains("quit")) {
          startable.shutdown();
        } else {
          System.out.println("Type 'quit' for closing the client");
        }
      }
    };
  }

  private static ConnectionHandler getConnectionHandler(Settings settings,
      CommandLineArguments arguments) throws ReflectiveOperationException, IOException {
    return new ConnectionHandler(settings.getHost(), settings.getPort(),
        arguments.getUsername(), arguments.getPassword());
  }

  private static DirectoryListener getDirectoryListener(Settings settings,
      Callable<Void, TreeNode<IMeta>> treeNodeCallable) throws IOException {
    DirectoryChangeHandler changeHandler = new DirectoryChangeHandler(settings,
        treeNodeCallable);
    DirectoryListener listener = new DirectoryListener(settings.getWorkingDir(),
        changeHandler);
    return listener;
  }

}
