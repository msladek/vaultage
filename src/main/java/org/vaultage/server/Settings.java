package org.vaultage.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import ch.marcsladek.commons.util.properties.Properties;
import ch.marcsladek.commons.util.properties.Property;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class Settings {

  private static final Logger LOGGER = LogFactory.getLogger(Settings.class);

  private static final String USER_HOME = System.getProperty("user.home");
  private static final Path SETTINGS_DIR = Paths.get(USER_HOME, ".vaultage");
  private static final Path SETTINGS = SETTINGS_DIR.resolve("vaultage_server.properties");

  private static final Property<Integer> PROP_PORT = new Property<Integer>("port", 12963);
  private static final Property<Integer> PROP_CLIENT_TIMEOUT = new Property<Integer>(
      "client-timeout", 0);
  private static final Property<String> PROP_DATABASE = new Property<String>("database",
      SETTINGS_DIR.resolve("database/vaultage_dev").toString());
  private static final Property<String> PROP_DATABASE_USER = new Property<String>(
      "database-user", "sa");
  private static final Property<String> PROP_DATABASE_PASSWORD = new Property<String>(
      "database-password", "");
  private static final Property<Integer> PROP_INPUT_INTERVAL = new Property<Integer>(
      "input-interval", 5);
  private static final Property<String> PROP_LOGGING_DIR = new Property<String>(
      "logging-directory", SETTINGS_DIR.resolve("logs").toString());

  private static List<Property<? extends Object>> getProperties() {
    List<Property<? extends Object>> list = new ArrayList<Property<? extends Object>>();
    list.add(PROP_PORT);
    list.add(PROP_CLIENT_TIMEOUT);
    list.add(PROP_DATABASE);
    list.add(PROP_DATABASE_USER);
    list.add(PROP_DATABASE_PASSWORD);
    list.add(PROP_INPUT_INTERVAL);
    list.add(PROP_LOGGING_DIR);
    return list;
  }

  private final Properties properties;

  Settings() throws IOException, IllegalArgumentException {
    checkAndCreateDir(SETTINGS_DIR);
    LOGGER.info("Settings directory: " + SETTINGS_DIR);
    properties = new Properties(SETTINGS, getProperties());
    checkAndCreateDir(getLoggingDir());
  }

  private void checkAndCreateDir(Path dir) throws IOException {
    if (!Files.exists(dir)) {
      Files.createDirectories(dir);
      LOGGER.debug("Directory " + dir + " has been created.");
    } else if (!Files.isDirectory(dir)) {
      throw new IOException("Path is expected to be a Directory, but is in fact a File: "
          + dir);
    }
  }

  public Path getSettingsDir() {
    return SETTINGS_DIR;
  }

  public int getPort() {
    return (Integer) properties.getPropertyValue(PROP_PORT);
  }

  public int getClientTimeout() {
    return (Integer) properties.getPropertyValue(PROP_CLIENT_TIMEOUT);
  }

  public String getDatabase() {
    return (String) properties.getPropertyValue(PROP_DATABASE);
  }

  public String getDatabaseUser() {
    return (String) properties.getPropertyValue(PROP_DATABASE_USER);
  }

  public String getDatabasePasword() {
    return (String) properties.getPropertyValue(PROP_DATABASE_PASSWORD);
  }

  public int getInputInterval() {
    return (Integer) properties.getPropertyValue(PROP_INPUT_INTERVAL);
  }

  public Path getLoggingDir() {
    return Paths.get((String) properties.getPropertyValue(PROP_LOGGING_DIR));
  }

}
