package org.vaultage.server.core;

import org.vaultage.commons.file.FileData;
import org.vaultage.commons.file.meta.IMeta;

import ch.marcsladek.commons.util.tree.TreeNode;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class DataHandler {

  private static final Logger LOGGER = LogFactory.getLogger(DataHandler.class);

  public void handleTree(String clientID, TreeNode<?> tree) {
    LOGGER.info("Received '" + tree.toStringSimple() + "' from '" + clientID + "'");
    Object element = tree.getElement();
    if (element instanceof IMeta) {
      // TODO
    } else if (element instanceof FileData) {
      // TODO
    } else {
      // TODO
    }
  }

}
