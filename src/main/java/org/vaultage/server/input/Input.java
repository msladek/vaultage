package org.vaultage.server.input;

public final class Input {

  private final String clientID;
  private final Object inputObject;

  public Input(String clientID, Object inputObject) {
    this.clientID = clientID;
    this.inputObject = inputObject;
  }

  String getClientID() {
    return clientID;
  }

  Object getInputObject() {
    return inputObject;
  }

}