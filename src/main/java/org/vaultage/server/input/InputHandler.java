package org.vaultage.server.input;

import java.util.List;
import java.util.Objects;

import org.vaultage.commons.auth.Auth;
import org.vaultage.commons.auth.AuthResponse;
import org.vaultage.server.OutputHandler;
import org.vaultage.server.core.DataHandler;
import org.vaultage.server.database.Authoriser;

import ch.marcsladek.commons.concurrent.callable.Callable;
import ch.marcsladek.commons.util.tree.TreeNode;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public final class InputHandler implements Callable<Void, List<Input>> {

  private static final Logger LOGGER = LogFactory.getLogger(InputHandler.class);

  private final Authoriser authoriser;
  private final DataHandler dataHandler;
  private final OutputHandler outputHandler;

  public InputHandler(Authoriser authoriser, DataHandler dataHandler,
      OutputHandler outputHandler) {
    this.authoriser = Objects.requireNonNull(authoriser);
    this.dataHandler = Objects.requireNonNull(dataHandler);
    this.outputHandler = Objects.requireNonNull(outputHandler);
  }

  @Override
  public Void call(List<Input> inputs) {
    if (isValidInput(inputs)) {
      for (Input in : inputs) {
        String clientID = in.getClientID();
        Object object = in.getInputObject();
        if (object instanceof Auth) {
          checkLock(clientID, (Auth) object);
        } else if (object instanceof TreeNode) {
          dataHandler.handleTree(clientID, (TreeNode<?>) object);
        } else {
          LOGGER.warn("Received illegal Object '" + object.toString()
              + "' from Connection '" + clientID + "'");
        }
      }
    }
    return null;
  }

  private boolean isValidInput(List<Input> inputs) {
    try {
      if (inputs != null) {
        for (Input in : inputs) {
          if (in == null) {
            return false;
          }
        }
      } else {
        return false;
      }
      return true;
    } catch (Exception exc) {
      LOGGER.error("Error occured while checking input", exc);
      return false;
    }
  }

  private void checkLock(String clientID, Auth auth) {
    AuthResponse response = authoriser.authorise(auth, clientID);
    outputHandler.handleOutput(clientID, response);
  }

}
