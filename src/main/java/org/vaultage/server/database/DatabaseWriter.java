package org.vaultage.server.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.vaultage.server.database.table.Column;
import org.vaultage.server.database.table.TableRow;

public class DatabaseWriter {

  private final Database db;

  public DatabaseWriter(Database db) {
    this.db = Objects.requireNonNull(db);
  }

  public int write(TableRow row) throws SQLException {
    String sql = getSQL(row);
    PreparedStatement pStatement = db.prepareStatement(sql);
    bindFileValues(pStatement, row);
    return db.update(pStatement, 1);
  }

  private String getSQL(TableRow row) {
    String fields = "", values = "", del = "";
    for (Column column : row.getColumnDef()) {
      fields += del + column.getFieldName();
      values += del + "?";
      del = ",";
    }
    return "insert into " + row.getTableName() + " (" + fields + ") values (" + values
        + ")";
  }

  private void bindFileValues(PreparedStatement prepareStatement, TableRow row)
      throws SQLException {
    List<Column> columns = row.getColumnDef();
    for (int i = 0; i < columns.size(); i++) {
      bindValue(prepareStatement, i + 1, columns.get(i).getSQLType(), row.getValue(i));
    }
  }

  private void bindValue(PreparedStatement prepareStatement, int index, int sqlType,
      Object object) throws SQLException {
    if (object != null) {
      switch (sqlType) {
        case Types.INTEGER :
          prepareStatement.setInt(index, (int) object);
          break;
        case Types.VARCHAR :
          prepareStatement.setString(index, (String) object);
          break;
        case Types.TIMESTAMP :
          prepareStatement.setTimestamp(index, new Timestamp(((Date) object).getTime()));
          break;
        case Types.LONGVARBINARY :
          prepareStatement.setBytes(index, (byte[]) object);
          break;
        default :
          throw new IllegalArgumentException("Unknown sqlType '" + sqlType + "'");
      }
    } else {
      prepareStatement.setNull(index, sqlType);
    }
  }
}
