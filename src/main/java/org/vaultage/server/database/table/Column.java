package org.vaultage.server.database.table;

import java.util.Objects;

public class Column {

  private final String fieldName;
  private final int sqlType;

  Column(String fieldName, int sqlType) {
    this.fieldName = fieldName;
    this.sqlType = sqlType;
  }

  public String getFieldName() {
    return this.fieldName;
  }

  public int getSQLType() {
    return sqlType;
  }

  @Override
  public int hashCode() {
    return Objects.hash(fieldName, sqlType);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Column) {
      Column other = (Column) obj;
      return Objects.equals(fieldName, other.fieldName)
          && Objects.equals(sqlType, other.sqlType);
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "Column [fieldName=" + fieldName + ", sqlType=" + sqlType + "]";
  }

}