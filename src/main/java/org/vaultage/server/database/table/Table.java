package org.vaultage.server.database.table;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum Table {

  USER, TREE, DATA;

  private static final List<Column> userColumnDef;
  private static final List<Column> treeColumnDef;
  private static final List<Column> dataColumnDef;
  static {
    List<Column> list = new ArrayList<>();
    list.add(new Column("NAME", Types.VARCHAR));
    list.add(new Column("HASH", Types.VARCHAR));
    userColumnDef = Collections.unmodifiableList(list);
    list = new ArrayList<>();
    list.add(new Column("FK_USER", Types.INTEGER));
    list.add(new Column("DATE", Types.TIMESTAMP));
    list.add(new Column("TREE", Types.LONGVARBINARY));
    treeColumnDef = Collections.unmodifiableList(list);
    list = new ArrayList<>();
    list.add(new Column("FK_USER", Types.INTEGER));
    list.add(new Column("HASH", Types.VARCHAR));
    list.add(new Column("DATA", Types.LONGVARBINARY));
    dataColumnDef = Collections.unmodifiableList(list);
  }

  static List<Column> getColumnDef(Table table) {
    switch (table) {
      case USER :
        return userColumnDef;
      case TREE :
        return treeColumnDef;
      case DATA :
        return dataColumnDef;
      default :
        throw new IllegalArgumentException("Unknown table '" + table + "'");
    }
  }

}
