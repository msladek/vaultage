package org.vaultage.server.database.table;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TableRow {

  private final Table table;
  private final List<Object> values;

  public TableRow(Table table, Object... rowValues) {
    this.table = table;
    this.values = new ArrayList<>();
    List<Column> columnDefs = getColumnDef();
    for (int i = 0; i < columnDefs.size(); i++) {
      String msg = checkValues(columnDefs.get(i), i, rowValues);
      if (msg == null) {
        this.values.add(rowValues[i]);
      } else {
        throw new IllegalArgumentException(msg);
      }
    }
  }

  private String checkValues(Column column, int index, Object... rowValues) {
    if (index < rowValues.length) {
      Object value = rowValues[index];
      if (value != null) {
        String msg = "For '" + column + "': value '" + value + "' ";
        switch (column.getSQLType()) {
          case Types.INTEGER :
            return value instanceof Integer ? null : msg + "not of type Integer";
          case Types.VARCHAR :
            return value instanceof String ? null : msg + "not of type String";
          case Types.TIMESTAMP :
            return value instanceof Date ? null : msg + "not of type Date/Timestamp";
          case Types.LONGVARBINARY :
            return value instanceof byte[] ? null : msg + "not of type byte[]";
          default :
            return msg + "of invalid type";
        }
      } else {
        return null;
      }
    } else {
      return "Not enough row values for columns";
    }
  }

  public String getTableName() {
    return table.name();
  }

  public List<Column> getColumnDef() {
    return Table.getColumnDef(table);
  }

  public List<Object> getValues() {
    return new ArrayList<>(values);
  }

  public Object getValue(int index) {
    return values.get(index);
  }

}
