package org.vaultage.server.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

import org.vaultage.commons.auth.Auth;
import org.vaultage.commons.auth.AuthResponse;
import org.vaultage.server.comm.UserManager;
import org.vaultage.server.database.table.Table;

import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public final class Authoriser {

  private static final Logger LOGGER = LogFactory.getLogger(Authoriser.class);

  private static final String SQL = "select USER_ID from " + Table.USER
      + " where NAME=? and HASH=?";

  // TODO DBReader!
  private final Database db;
  private final UserManager userManager;

  public Authoriser(Database db, UserManager userManager) {
    this.db = Objects.requireNonNull(db);
    this.userManager = Objects.requireNonNull(userManager);
  }

  public AuthResponse authorise(Auth auth, String clientID) {
    AuthResponse response = AuthResponse.FAILURE;
    try {
      PreparedStatement pStatement = db.prepareStatement(SQL);
      pStatement.setString(1, auth.getUsername());
      pStatement.setString(2, auth.getHash());
      List<Object[]> result = db.query(pStatement);
      int id = getAndCast(result);
      if ((id >= 0) && userManager.unlock(clientID, id)) {
        response = AuthResponse.SUCCESS;
      }
    } catch (SQLException exc) {
      LOGGER.error("Error while executing query on Database '" + db + "' for client '"
          + clientID + "'", exc);
    }
    LOGGER.debug("Authorisation for username '" + auth.getUsername() + "' and client '"
        + clientID + "' was a " + response);
    return response;
  }

  private int getAndCast(List<Object[]> result) {
    int ret = -1;
    if ((result != null) && (result.size() > 0) && (result.get(0).length > 0)) {
      Object obj = result.get(0)[0];
      if (obj instanceof Integer) {
        ret = (Integer) obj;
      }
    }
    return ret;
  }

}
