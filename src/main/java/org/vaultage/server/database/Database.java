package org.vaultage.server.database;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.io.FileUtils;

import ch.marcsladek.commons.concurrent.startable.Startable;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

/**
 * 
 * URL for Server: jdbc:hsqldb:hsql://localhost/vaultage <br>
 * URL for Standalone: jdbc:hsqldb:file:database/vaultage <br>
 * <br>
 * Server startup (not needed when Standalone): <br>
 * java -cp lib/hsqldb-2.2.9/hsqldb.jar org.hsqldb.Server -database.0
 * file:database/vaultage -dbname.0 vaultage <br>
 * <br>
 * DatabaseManager: <br>
 * java -cp lib/hsqldb-2.2.9/hsqldb.jar org.hsqldb.util.DatabaseManagerSwing <br>
 * <br>
 * SQL Syntax: <br>
 * http://www.hsqldb.org/doc/guide/ch09.html<br>
 * 
 */
public class Database implements Startable {

  private static final Logger LOGGER = LogFactory.getLogger(Database.class);

  private static final String DRIVER = "org.hsqldb.jdbcDriver";

  private final String database, user, password;
  private Connection connection;
  private final File sqlFile;

  public Database(String database, String user, String password, File sqlFile)
      throws SQLException {
    this.database = Objects.requireNonNull(database);
    this.user = Objects.requireNonNull(user);
    this.password = Objects.requireNonNull(password);
    this.sqlFile = sqlFile;
    try {
      Class.forName(DRIVER);
    } catch (ClassNotFoundException exc) {
      throw new SQLException("Unable to load database driver '" + DRIVER + "'");
    }
  }

  @Override
  public void start() throws Exception {
    try {
      setConnection(database, user, password);
      setUpDatabase(sqlFile);
      LOGGER.debug("Database connection was successful");
    } catch (Exception exc) {
      closeConnection();
      throw exc;
    }
  }

  private void setUpDatabase(File sqlFile) throws SQLException, IOException {
    if (sqlFile != null) {
      String sqls = FileUtils.readFileToString(sqlFile, "UTF-8");
      sqls = sqls.replace("\n", "").replace("\r", "");
      for (String sql : sqls.split(";")) {
        update(prepareStatement(sql));
      }
    }
  }

  @Override
  public boolean isRunning() {
    return connection != null;
  }

  @Override
  public synchronized void shutdown() {
    LOGGER.info("Database shutdown");
    try {
      getConnection().createStatement().execute("SHUTDOWN");
    } catch (SQLException | IllegalStateException exc) {
      LOGGER.error("Unable to execute shutdown on database connection", exc);
    } finally {
      closeConnection();
    }
  }

  @Override
  public boolean isTerminated() {
    return connection == null;
  }

  public synchronized PreparedStatement prepareStatement(String sql) throws SQLException {
    return getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
  }

  public int update(PreparedStatement pStatement) throws SQLException {
    return update(pStatement, -1);
  }

  public synchronized int update(PreparedStatement pStatement, int key)
      throws SQLException {
    LOGGER.debug("Executing update '" + pStatement + "' for key '" + key + "'");
    try {
      int affectedRows = pStatement.executeUpdate();
      if (affectedRows >= 0) {
        return key > 0 ? getGeneratedKey(pStatement, key) : affectedRows;
      } else {
        throw new SQLException("Database error while executing statement '" + pStatement
            + "'");
      }
    } finally {
      closeStatement(pStatement);
    }
  }

  private int getGeneratedKey(PreparedStatement pStatement, int key) throws SQLException {
    ResultSet generatedKeys = pStatement.getGeneratedKeys();
    if (generatedKeys.next()) {
      return generatedKeys.getInt(key);
    } else {
      return -1;
    }
  }

  public synchronized List<Object[]> query(PreparedStatement pStatement)
      throws SQLException {
    LOGGER.debug("Executing query '" + pStatement + "'");
    List<Object[]> result = new ArrayList<Object[]>();
    try {
      ResultSet rs = pStatement.executeQuery();
      int columns = rs.getMetaData().getColumnCount();
      while (rs.next()) {
        Object[] row = new Object[columns];
        for (int i = 0; i < columns; i++) {
          row[i] = rs.getObject(i + 1);
        }
        result.add(row);
      }
    } finally {
      closeStatement(pStatement);
    }
    return result;
  }

  private Connection getConnection() throws SQLException {
    if (connection != null) {
      if (!connection.isValid(5)) {
        LOGGER.warn("Invalid connection, reestablishing");
        setConnection(database, user, password);
      }
      return connection;
    } else {
      throw new IllegalStateException("Unable to access database connection, "
          + "initalization error occured");
    }
  }

  private void setConnection(String database, String user, String password)
      throws SQLException {
    closeConnection();
    connection = DriverManager.getConnection("jdbc:hsqldb:file:" + database, user,
        password);
  }

  private void closeConnection() {
    if (connection != null) {
      try {
        connection.close();
      } catch (SQLException exc) {
        // silent catch
      }
      connection = null;
    }
  }

  private void closeStatement(Statement statement) {
    if (statement != null) {
      try {
        statement.close();
      } catch (SQLException e) {
        // silent catch
      }
    }
  }

}
