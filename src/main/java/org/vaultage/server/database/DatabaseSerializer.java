package org.vaultage.server.database;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.SerializationUtils;

public class DatabaseSerializer<T extends Serializable> {

  private final Database db;
  private final String sqlInsert, sqlSelect;

  public DatabaseSerializer(Database db, String tableName) {
    this.db = Objects.requireNonNull(db);
    this.sqlInsert = "INSERT INTO " + tableName + " (data) VALUES (?)";
    this.sqlSelect = "SELECT data FROM " + tableName + " WHERE id=?";
  }

  public int write(T object) throws SQLException {
    byte[] data = SerializationUtils.serialize(object);
    PreparedStatement pStatement = db.prepareStatement(sqlInsert);
    pStatement.setBytes(1, data);
    return db.update(pStatement, 1);
  }

  @SuppressWarnings("unchecked")
  public T read(int id) throws SQLException {
    PreparedStatement pStatement = db.prepareStatement(sqlSelect);
    pStatement.setInt(1, id);
    List<Object[]> result = db.query(pStatement);
    byte[] data = getData(result);
    return (T) SerializationUtils.deserialize(data);
  }

  private byte[] getData(List<Object[]> result) {
    byte[] data = null;
    if ((result != null) && (result.size() > 0) && (result.get(0).length > 0)) {
      data = (byte[]) result.get(0)[0];
    }
    return data;
  }

}
