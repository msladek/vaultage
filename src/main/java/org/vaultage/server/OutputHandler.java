package org.vaultage.server;

import java.io.IOException;
import java.util.Objects;

import org.vaultage.commons.auth.AuthResponse;
import org.vaultage.server.comm.UserManager;

import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public final class OutputHandler {

  private static final Logger LOGGER = LogFactory.getLogger(OutputHandler.class);

  private final UserManager userManager;

  OutputHandler(UserManager userManager) {
    this.userManager = Objects.requireNonNull(userManager);
  }

  public void handleOutput(String clientID, Object obj) {
    LOGGER.debug("handleOutput: " + clientID + " " + obj);
    try {
      if (obj instanceof AuthResponse) {
        userManager.send(clientID, obj);
      } else {
        LOGGER.warn("Trying to send illegal Object '" + obj.toString()
            + "' to Connection '" + clientID + "'");
      }
    } catch (IOException exc) {
      LOGGER.error("Cannot send Object '" + obj + "' to Connection '" + clientID + "'",
          exc);
    }
  }
}
