package org.vaultage.server;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.vaultage.server.comm.ConnectionHandler;
import org.vaultage.server.comm.UserManager;
import org.vaultage.server.core.DataHandler;
import org.vaultage.server.database.Authoriser;
import org.vaultage.server.database.Database;
import org.vaultage.server.input.Input;
import org.vaultage.server.input.InputHandler;

import ch.marcsladek.commons.concurrent.callDelayer.CallDelayer;
import ch.marcsladek.commons.concurrent.startable.Startable;
import ch.marcsladek.commons.concurrent.startable.Startup;
import ch.marcsladek.commons.io.ConsoleReader;
import ch.marcsladek.yajlf.ErrorLevel;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class Server {

  private static final Logger LOGGER = LogFactory.getLogger(Server.class);

  public static final boolean DEBUG = true;
  private static final String LOG_FILE = "log-server.txt";

  private static Startable startable;

  public static void main(String[] args) throws Exception {

    Settings settings = new Settings();
    setLog(settings);

    ConsoleReader consoleReader = getConsoleReader();
    Database database = getDatabase(settings);
    ConnectionHandler connHandler = getConnectionHandler(settings);
    UserManager userManager = connHandler.getUserManager();
    Authoriser authoriser = new Authoriser(database, userManager);
    DataHandler dataHandler = new DataHandler();
    OutputHandler outputHandler = new OutputHandler(userManager);
    InputHandler inputHandler = new InputHandler(authoriser, dataHandler, outputHandler);
    CallDelayer<Input> callDelayer = getCallDelayer(settings, inputHandler);
    userManager.setInputCallable(callDelayer);

    startable = new Startup(database, callDelayer, connHandler, userManager,
        consoleReader);
    startable.start();
    LOGGER.info("Server started. Listening to port " + settings.getPort());
  }

  private static void setLog(Settings settings) throws IOException {
    LogFactory.setLog(settings.getLoggingDir().resolve(LOG_FILE).toString(),
        DEBUG ? ErrorLevel.TRACE : ErrorLevel.INFO);
  }

  private static ConsoleReader getConsoleReader() {
    return new ConsoleReader() {
      @Override
      protected void process(String line) {
        if (line.toLowerCase().contains("quit")) {
          startable.shutdown();
        } else {
          System.out.println("Type 'quit' for closing the client");
        }
      }
    };
  }

  private static Database getDatabase(Settings settings) throws SQLException {
    Database database = new Database(settings.getDatabase(), settings.getDatabaseUser(),
        settings.getDatabasePasword(), new File("src/main/resources/db_vaultage_dev.sql"));
    return database;
  }

  private static ConnectionHandler getConnectionHandler(Settings settings)
      throws ReflectiveOperationException, IOException {
    return new ConnectionHandler(settings.getPort(), settings.getClientTimeout());
  }

  private static CallDelayer<Input> getCallDelayer(Settings settings,
      InputHandler inputHandler) {
    return new CallDelayer<>(inputHandler, settings.getInputInterval(), TimeUnit.SECONDS);
  }

}
