package org.vaultage.server.comm;

import java.io.IOException;

import ch.marcsladek.commons.concurrent.startable.Startable;
import ch.marcsladek.jrtnp.server.Server;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class ConnectionHandler extends Server implements Startable {

  private static final Logger LOGGER = LogFactory.getLogger(ConnectionHandler.class);

  public ConnectionHandler(int port, int timeout) throws ReflectiveOperationException,
      IOException {
    super(port, timeout, false, ClientConnectionFactory.class, UserManagerFactory.class);
  }

  public UserManager getUserManager() {
    return (UserManager) clientManager;
  }

  @Override
  public void started() {
    LOGGER.info("ConnectionHandler started");
  }

  @Override
  public void shuttedDown() {
    LOGGER.info("ConnectionHandler shutdown");
  }

  @Override
  public boolean isRunning() {
    return serverSocketListener.isListening();
  }

  @Override
  public boolean isTerminated() {
    return !serverSocketListener.isListening();
  }

}
