package org.vaultage.server.comm;

import ch.marcsladek.jrtnp.connection.ConnectionFactory;
import ch.marcsladek.jrtnp.server.ClientManager;
import ch.marcsladek.jrtnp.server.ClientManagerFactory;

public class UserManagerFactory implements ClientManagerFactory {

  @Override
  public ClientManager newInstance(
      Class<? extends ConnectionFactory> connectionFactoryClass)
      throws ReflectiveOperationException {
    return new UserManager(connectionFactoryClass);
  }

}
