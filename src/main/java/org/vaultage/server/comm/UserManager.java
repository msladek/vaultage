package org.vaultage.server.comm;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

import org.vaultage.server.input.Input;

import ch.marcsladek.commons.concurrent.callable.Callable;
import ch.marcsladek.commons.concurrent.startable.Startable;
import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionFactory;
import ch.marcsladek.jrtnp.server.ClientManager;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public class UserManager extends ClientManager implements Startable {

  private static final Logger LOGGER = LogFactory.getLogger(UserManager.class);

  private final ConcurrentHashMap<String, Integer> unlockMap;

  protected UserManager(Class<? extends ConnectionFactory> connectionFactoryClass)
      throws ReflectiveOperationException {
    super(connectionFactoryClass);
    unlockMap = new ConcurrentHashMap<>();
  }

  public void setInputCallable(Callable<Void, Input> inputCallable) {
    if (!((ClientConnectionFactory) factory).setInputCallable(inputCallable)) {
      throw new IllegalStateException("InputCallable already set");
    }
  }

  public boolean unlock(String clientID, int userID) {
    if (unlockMap.putIfAbsent(clientID, userID) == null) {
      return true;
    } else {
      LOGGER.warn("Duplicate key '" + clientID + "' for unlockMap. Current value: "
          + userID + ", failed values: " + unlockMap.get(clientID));
      return false;
    }
  }

  public boolean isUnlocked(String clientID) {
    return unlockMap.contains(clientID);
  }

  @Override
  protected void newClientNotExistent(Connection newClient) {
    LOGGER.info("New Connection '" + newClient + "'");
    newClient.start();
  }

  @Override
  protected void newClientExistent(Connection newClient) {
    LOGGER.warn("Connection '" + newClient + "' already exists");
  }

  @Override
  protected void newClientIOException(Socket socket, IOException exp) {
    LOGGER.error("Connecting to '" + socket + "' failed", exp);
  }

  @Override
  protected boolean removeClientRemoved(Connection client) {
    LOGGER.info("Connection '" + client + "' removed and shutted down");
    client.shutdown();
    return true;
  }

  @Override
  protected boolean removeClientNotRemoved(Connection client) {
    LOGGER.warn("Connection '" + client + "' not found for removal");
    return false;
  }

  @Override
  public void start() throws Exception {}

  @Override
  public boolean isRunning() {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean isTerminated() {
    throw new UnsupportedOperationException();
  }

}
