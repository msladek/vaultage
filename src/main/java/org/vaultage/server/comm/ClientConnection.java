package org.vaultage.server.comm;

import java.io.IOException;
import java.net.Socket;
import java.util.Objects;

import org.vaultage.server.input.Input;

import ch.marcsladek.commons.concurrent.callable.Callable;
import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionException;
import ch.marcsladek.yajlf.LogFactory;
import ch.marcsladek.yajlf.Logger;

public final class ClientConnection extends Connection {

  private static final Logger LOGGER = LogFactory.getLogger(ClientConnection.class);

  private final Callable<Void, Input> inputCallable;

  ClientConnection(Socket socket, Callable<Void, Input> inputCallable) throws IOException {
    super(Objects.requireNonNull(socket));
    this.inputCallable = Objects.requireNonNull(inputCallable);
  }

  @Override
  protected void process(Object obj) {
    inputCallable.call(new Input(getIdentifier(), obj));
  }

  @Override
  protected void finish(boolean closedRemotely, ConnectionException closeException) {
    // TODO finish processing
    LOGGER.error("ClientConnection.finish(): implementation needed");
    LOGGER.trace("Closed Connection '" + getIdentifier() + "' "
        + (closedRemotely ? "remotely" : ""), closeException);
  }

}
