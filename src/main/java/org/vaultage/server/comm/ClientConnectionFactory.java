package org.vaultage.server.comm;

import java.io.IOException;
import java.net.Socket;
import java.util.Objects;

import org.vaultage.server.input.Input;

import ch.marcsladek.commons.concurrent.callable.Callable;
import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionFactory;

public final class ClientConnectionFactory implements ConnectionFactory {

  private Callable<Void, Input> inputCallable;

  boolean setInputCallable(Callable<Void, Input> inputCallable) {
    if (this.inputCallable == null) {
      this.inputCallable = Objects.requireNonNull(inputCallable);
      return true;
    }
    return false;
  }

  @Override
  public Connection newInstance(Socket socket) throws IOException {
    return new ClientConnection(socket, inputCallable);
  }

}
